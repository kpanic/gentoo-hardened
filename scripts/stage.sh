#!/bin/bash

source /etc/profile

mkdir -p /mnt/gentoo/
mount -t ext4 -o rw,relatime,discard ${partn} /mnt/gentoo

FILE=$(wget -q http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64-hardened/ -O - | grep -o -e "stage3-amd64-hardened-\w*.tar.bz2" | uniq)
wget http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64-hardened/$FILE -O /mnt/gentoo/$FILE || exit 1
#wget http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64-hardened/$FILE.CONTENTS -O /mnt/gentoo/$FILE.CONTENTS
#wget http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64-hardened/$FILE.DIGESTS -O /mnt/gentoo/$FILE.DIGESTS
#wget http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64-hardened/$FILE.DIGESTS.asc -O /mnt/gentoo/$FILE.DIGESTS.asc

#gkeys verify /mnt/gentoo/$FILE.DIGESTS.asc || exit 1

tar -C /mnt/gentoo -jxpf /mnt/gentoo/$FILE
rm -f /mnt/gentoo/$FILE

sync