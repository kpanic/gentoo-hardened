#!/bin/bash
source /etc/profile

chroot "$chroot" /bin/bash -eux<<DATAEOF

cd /usr/src/linux && make clean && make mrproper
emerge -C sys-kernel/hardened-sources
emerge --depclean
eselect news read

DATAEOF

rm -rf /mnt/gentoo/usr/portage
rm -rf /mnt/gentoo/tmp/*
rm -rf /mnt/gentoo/var/log/*
rm -rf /mnt/gentoo/var/tmp/*
rm -f  /mnt/gentoo/root/.bash_history
rm -f /mnt/gentoo/root/.viminfo