#!/bin/bash
source /etc/profile

chroot "$chroot" /bin/bash -eux<<DATAEOF
source /etc/profile
env-update
emerge-webrsync
eselect profile set hardened/linux/amd64
emerge -quN portage
emerge -quDN world
perl-cleaner --reallyall
emerge --noreplace app-editors/nano
emerge --depclean
#echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
DATAEOF