#!/bin/bash
source /etc/profile

MAKECONF="$chroot"/etc/portage/make.conf
rm -f "$chroot"/etc/portage/make.conf
touch $MAKECONF

cat <<DATAEOF >> $MAKECONF
CFLAGS="-O2 -pipe"
CXXFLAGS="${CFLAGS}"
CHOST="x86_64-pc-linux-gnu"
USE="mmx sse sse2"
USE="${USE} -systemd -pulseaudio -handbook -bindist -X"
PORTDIR="/usr/portage"
DISTDIR="${PORTDIR}/distfiles"
PKGDIR="${PORTDIR}/packages"
LINGUAS="ru en ru_RU"
CURL_SSL="gnutls"
MAKEOPTS="-j5"
DATAEOF

cp -L /etc/resolv.conf "$chroot"/etc/

LOCALECONF="$chroot"/etc/locale.gen

rm -f $LOCALECONF

cat <<DATAEOF >> $LOCALECONF
en_US.UTF-8 UTF-8
ru_RU.UTF-8 UTF-8
DATAEOF

chroot "$chroot" /bin/bash -eux<<DATAEOF
echo Europe/Moscow > /etc/timezone
echo hostname="build" > /etc/conf.d/hostname
ln -s /etc/init.d/net.lo /etc/init.d/net.eth0
locale-gen
eselect locale set 4
rc-update add net.eth0 default
rc-update add sshd default
echo root:packer | chpasswd
DATAEOF