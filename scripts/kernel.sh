#!/bin/bash

source /etc/profile

chroot "$chroot" /bin/bash -eux<<DATAEOF

emerge sys-kernel/hardened-sources
cd /usr/src/linux
make defconfig
cp /tmp/file/kernel.conf /usr/src/linux/.config
cp /tmp/file/x509.genkey /usr/src/linux/certs/x509.genkey
openssl req -new -nodes -utf8 -sha256 -days 36500 -batch -x509 \
	-config certs/x509.genkey -outform PEM -out certs/kernel_key.pem \
	-keyout certs/kernel_key.pem
make -j5
make modules_install
make install
emerge sys-kernel/genkernel
genkernel --no-lvm --no-mdadm --no-dmraid --no-zfs --no-btrfs --no-luks --no-gpg --no-mountboot initramfs
DATAEOF