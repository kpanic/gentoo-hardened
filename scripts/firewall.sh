#!/bin/bash

source /etc/profile

chroot "$chroot" /bin/bash -eux<<DATAEOF
mkdir -p /etc/portage/package.accept_keywords
echo "net-firewall/xtables-addons ~amd64" >> /etc/portage/package.accept_keywords/xtables
echo "net-firewall/ipset -modules" >> /etc/portage/package.use/ipset
echo "net-misc/iprange ~amd64" >> /etc/portage/package.accept_keywords/iprange
emerge net-firewall/xtables-addons net-firewall/ipset net-misc/iprange
DATAEOF