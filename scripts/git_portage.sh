#!/bin/bash
source /etc/profile


chroot "$chroot" /bin/bash -eux<<DATAEOF
mkdir /etc/portage/repos.conf
emerge dev-vcs/git
emerge eix
mkdir -p /etc/portage/repos.conf/
cp /tmp/file/gentoo.conf /etc/portage/repos.conf/
rm -rf /usr/portage
eix-sync
DATAEOF