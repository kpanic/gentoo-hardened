#!/bin/bash
echo "drive=/dev/sda" >> /etc/profile
echo "partn=/dev/sda1" >> /etc/profile
echo "chroot=/mnt/gentoo" >> /etc/profile
source /etc/profile

mkdir -p /mnt/gentoo/

parted -s ${drive} "mklabel msdos"
parted -s ${drive} "mkpart primary ext4 2 100%"
parted -s ${drive} "set 1 boot"

mkfs.ext4 ${partn}
