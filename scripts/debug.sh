#!/bin/bash
source /etc/profile

drive="/dev/sda"
partn="/dev/sda1"
mkdir -p /mnt/gentoo/

parted -s ${drive} "mklabel msdos"
parted -s ${drive} "mkpart primary ext4 2 100%"
parted -s ${drive} "set 1 boot"

sync

mkfs.ext4 ${partn}
mount -t ext4 -o rw,relatime,discard ${partn} /mnt/gentoo

FILE=$(wget -q http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64-hardened/ -O - | grep -o -e "stage3-amd64-hardened-\w*.tar.bz2" | uniq)
wget http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64-hardened/$FILE -O /mnt/gentoo/stage.tar.bz2 || exit 1

tar -C /mnt/gentoo -jxvpf /mnt/gentoo/stage.tar.bz2
rm -f /mnt/gentoo/stage.tar.bz2
sync

cp -L /etc/resolv.conf /mnt/gentoo/etc/

echo hostname="build" > /mnt/gentoo/etc/conf.d/hostname
echo -e "
/dev/sda1    /           ext4        defaults,discard,relatime         0 1
" >> /mnt/gentoo/etc/fstab

sed -i '/\/dev\/BOOT.*/d' /mnt/gentoo/etc/fstab
sed -i '/\/dev\/ROOT.*/d' /mnt/gentoo/etc/fstab
sed -i '/\/dev\/SWAP.*/d' /mnt/gentoo/etc/fstab

mount -t proc proc /mnt/gentoo/proc
mount -o bind /sys /mnt/gentoo/sys
mount -o rbind /dev /mnt/gentoo/dev

MAKECONF=/mnt/gentoo/etc/portage/make.conf
[ ! -f $MAKECONF ] && [ -f /mnt/gentoo/etc/make.conf ] && MAKECONF=/mnt/gentoo/etc/make.conf
echo $MAKECONF

cat <<DATAEOF >> $MAKECONF
MAKEOPTS="-j5"
FEATURES="\${FEATURES} parallel-fetch"
USE="${USE} -X -bindist idn iproute2"
CFLAGS="-mtune=generic -O2 -pipe"
CXXFLAGS="\${CFLAGS}"
LINGUAS="ru en ru_RU"
DATAEOF

chroot /mnt/gentoo /bin/bash -ex<<DATAEOF

echo UTC > /etc/timezone
source /etc/profile
env-update
emerge-webrsync
eselect profile set hardened/linux/amd64

ln -s /etc/init.d/net.lo /etc/init.d/net.eth0
rc-update add net.eth0 default
ln -sf /usr/share/zoneinfo/Etc/UTC /etc/localtime
emerge --sync
echo 'GRUB_PLATFORMS="pc qemu"' >> /etc/portage/make.conf
emerge --nospinner grub
sed -i 's/^#\s*GRUB_CMDLINE_LINUX=.*/GRUB_CMDLINE_LINUX="net.ifnames=0"/' /etc/default/grub
emerge --nospinner openssh
rc-update add sshd default
sed -i -e 's|*.PermitRootLogin.*|PermitRootLogin yes|g' /etc/ssh/sshd_config
echo root:packer | chpasswd
DATAEOF

emerge -quN portage
emerge -quDN world
emerge --noreplace app-editors/nano
emerge --depclean

exit