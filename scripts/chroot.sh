#!/bin/bash

source /etc/profile
rm -f "$chroot"/etc/fstab

cp /tmp/file/fstab.conf "$chroot"/etc/fstab

mount -t ext4 -o rw,relatime,discard ${partn} "$chroot"
mount -t proc proc "$chroot"/proc
mount -o bind /sys "$chroot"/sys
mount -o rbind /dev "$chroot"/dev
mount -o bind /tmp "$chroot"/tmp
