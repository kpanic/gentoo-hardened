#!/bin/bash
source /etc/profile

chroot "$chroot" /bin/bash -eux<<DATAEOF
emerge --nospinner grub
sed -i 's/^#\s*GRUB_CMDLINE_LINUX=.*/GRUB_CMDLINE_LINUX="net.ifnames=0"/' /etc/default/grub
grub2-install /dev/sda
grub2-mkconfig -o /boot/grub/grub.cfg
DATAEOF